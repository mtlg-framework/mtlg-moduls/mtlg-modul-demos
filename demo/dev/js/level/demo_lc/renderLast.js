/*
 * UI Elements Demo
*/
var lcLastDemo_init = function()
{
  var stage = MTLG.getStage();
  MTLG.setBackgroundColor('white');

  let textP = {text: "Congratulations, you were able to answer all questions correctly!", font: "50px Arial", color: "Black", sizeX: 1000, sizeY: 100, wrap: true};
  let text = MTLG.utils.uiElements.addText(textP);
  text.x = 300;
  text.y = 50;
  stage.addChild(text);


  let bProp = {font: 'Arial', text:"Finish", sizeX: 150, sizeY : 70};
  let finishButton = MTLG.utils.uiElements.addButton(bProp, function(){
    MTLG.lc.goToMenu();
  });
  finishButton.x = 300;
  finishButton.y = 800;
  stage.addChild(finishButton);

};

/*
 * This level has a priority of 1 if all answers were correct (plus and mult) and score exists.
 */
var checkLcLast = function(gameState){
  if(gameState && gameState.nextLevel && gameState.nextLevel == "lcDemo"){
    if(gameState.score){
      if((gameState.score)[0] == 4 && (gameState.score)[1] == 4){
        return 1;
      }
    }
  }
  return 0;
}

module.exports = { lcLastDemo_init, checkLcLast };
