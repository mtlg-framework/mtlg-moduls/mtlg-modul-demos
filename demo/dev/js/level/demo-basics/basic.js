/**
 * @Author: thiemo
 * @Date:   2018-09-09T11:39:08+02:00
 * @Last modified by:   thiemo
 * @Last modified time: 2018-09-10T16:10:00+02:00
 */



/*
 * manages drawing into playing fields of every area
 */
var field1_init = function() {

  //Login is over now, all players are registered with the MTLG framework
  //Use getPlayerNumber to get the number of players that are currently logged in.
  //and a simple task to start the game (drag yellow rectangle into white area)

  console.log("This players are logged in:");
  for (var i = 0; i < MTLG.getPlayerNumber(); i++) {
    console.log(MTLG.getPlayer(i));
  }

  console.log("Thie are the available game options:");
  console.log(MTLG.getOptions());

  drawField1();
};



/*
 * this function draws the first playing field in the passed area
 * @param area: area to draw in
 */
var drawField1 = function() {
  // get the stage where the shapes are added
  var stage = MTLG.getStageContainer();

  // set the background color
  MTLG.setBackgroundColor('blue');

  // create a Text
  let text = MTLG.lang.getString('title_field1');
  let textSize = '28';
  let textShape = MTLG.utils.gfx.getText(text, textSize + 'px Arial', 'black');
  textShape.x = MTLG.getOptions().width / 2 - textShape.getBounds().width / 2;
  textShape.y = MTLG.getOptions().height / 4;
  stage.addChild(textShape);

  // create a target zone
  let zone = MTLG.utils.gfx.getShape();
  let edgeZone = MTLG.getOptions().width / 6;
  zone.graphics.beginFill('white').drawRect(-edgeZone / 2, -edgeZone / 2, edgeZone, edgeZone);
  zone.setBounds(-edgeZone / 2, -edgeZone / 2, edgeZone / 2, edgeZone / 2);
  zone.x = 300;
  zone.y = 600;
  stage.addChild(zone);


  // create a draggable object
  let object = MTLG.utils.gfx.getShape();
  let edgeObject = MTLG.getOptions().width / 8;
  object.graphics.beginFill('yellow').drawRect(-edgeObject / 2, -edgeObject / 2, edgeObject, edgeObject);
  object.setBounds(-edgeObject / 2, -edgeObject / 2, edgeObject / 2, edgeObject / 2);
  object.x = 1400;
  object.y = 750;
  object.on('pressmove', draggable);
  object.on('pressup', hitObject);
  stage.addChild(object);


  function draggable(e) {
    let p = e.currentTarget.localToLocal(e.localX, e.localY, stage);
    e.currentTarget.set(p);
  }

  function hitObject(e) {
    let p = e.currentTarget.localToLocal(e.localX, e.localY, stage);
    e.currentTarget.set(p);

    let conv = zone.localToLocal(e.localX, e.localY, e.currentTarget);

    if (zone.hitTest(conv.x, conv.y)) {
      e.currentTarget.x = zone.x;
      e.currentTarget.y = zone.y;
      e.currentTarget.removeAllEventListeners();

      MTLG.lc.goToMenu();
    } else {
      object.x = 1400;
      object.y = 750;
    }
  }
};



/**
 * Function that checks if Feld01 should be the next level
 * @param the current game state (an object containing only nextLevel in this case)
 * @return a number in [0,1] that represents that possibility.
 */
var checkLevel1 = function(gameState){
  if(gameState && gameState.nextLevel && gameState.nextLevel == "basicDemo"){
    return 1;
  }
  return 0;
}
