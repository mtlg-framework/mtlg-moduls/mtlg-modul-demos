/*
 * UI Elements Demo
*/
var lcMultDemo_init = function()
{
  var stage = MTLG.getStage();
  MTLG.setBackgroundColor('white');

  let questions = [
    {question: "1*11", options: [1,11,3,4], correct: 1},
    {question: "3*6", options: [5,7,18,10], correct: 2},
    {question: "29*35", options: [46,54,64, 1015], correct: 3},
    {question: "123*321", options: [39483,445,555,666], correct: 0},
    {question: "2*6", options: [6,12,18,24], correct: 1},
    {question: "7*7", options: [45,47,49,51], correct: 2},
    {question: "9*8", options: [56,64,68,72], correct: 3},
    {question: "11*25", options: [275, 277, 285, 333], correct: 0},
  ];
  let answers = [0,0,0,0,0,0,0,0];
  let utils = MTLG.utils.uiElements;
  let qProps = {sizeX : 1000, lineHeight: 30, textColor: "black", firstColor: "red", secondColor: "white"};
  let currentHeight = 50;
  let textP = {text: "Too many mistakes with multiplication! Try this exercise before trying again.", font: currentHeight + "px Arial", color: "Black", sizeX: 1000, sizeY: currentHeight, wrap: true};
  let text = utils.addText(textP);
  text.x = 300;
  text.y = currentHeight;
  stage.addChild(text);
  currentHeight += currentHeight * 1.5;
  for(q of questions){
    qProps.description = q.question;
    qProps.options = q.options;
    let index = questions.indexOf(q);
    let radioB = utils.addRadioButton(qProps, function(newVal){
      answers[index] = newVal;
    });
    radioB.x = 300;
    radioB.y = currentHeight;
    stage.addChild(radioB);
    currentHeight += 3.5*qProps.lineHeight;
  }

  let bProp = {font: 'Arial', text:"Finish", sizeX: 150, sizeY : 70};
  let finishButton = utils.addButton(bProp, function(){
    let score = [0,0];
    let possibleScore = [0,8];
    for(q in questions){
      if(answers[q] == questions[q].correct){
        score[1]++;
      }
    }
    MTLG.lc.levelFinished({
      nextLevel: "lcDemo",
      score: score,
      possibleScore: possibleScore,
    });
  });
  finishButton.x = 300;
  finishButton.y = currentHeight;
  stage.addChild(finishButton);

};

/*
 * Check if this is the next level.
 * If the nextLevel is set to lcDemo and there were multiplication questions that were wrong in the
 * previous test, the number of wrong answers is normalized, multiplied by 0.75 and returned.
 * This means that the score will get higher the more quesitons are wrong, but only if the score
 * is set (the user already played a level).
 * Notice that the priority of "plus" questions are higher, as they are not multiplied by 0.75.
 */
var checkLcMult = function(gameState){
  if(gameState && gameState.nextLevel && gameState.nextLevel == "lcDemo"){
    if(gameState.score && gameState.possibleScore[1] != 0){
      return (gameState.possibleScore[1]-gameState.score[1]) *0.75 /gameState.possibleScore[1];
    }else{
      return 0;
    }
  }
  return 0;
}

module.exports = { lcMultDemo_init, checkLcMult };
