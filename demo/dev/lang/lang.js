/**
 * @Date:   2019-10-07T17:48:27+02:00
 * @Last modified time: 2020-03-05T10:44:19+01:00
 */



MTLG.lang.define({
  'en': {
    'one_player': 'One Player',
    'two_player': 'Two Players',
    'title_login': 'Type in User and password',
    'title_field1': 'Take the rectangle to the white area',
    'title_field2': 'Press the fields with the numbers in the right order...',
    'temp_feedback': 'Your needed time in milliseconds is: ',
    'feedback_text': 'ms needed',
    'demo_game' : 'Demo Game',
    'start!' : 'Start!',
    'return_to_menu' : 'Return to Menu',
    'demo_graph' : 'Graphs Demo',
    'demo_ui' : 'UI Demo',
    'demo_fbm' : 'Feedback',
    'demo_lc' : 'Lifecycle',
    'demo_collision' : 'Collision',
    'demo_movable' : 'Movable Objects',
    'demo_qrcode' : 'QR Code',
    'demo_inputKeyboards' : 'Input Keyboards',
    'brick1_description' : 'Custom Circle size',
    'brick2_description' : 'Rotates in steps',
    'brick3_description' : 'Rotates faster (2x)',
    'brick4_description' : 'Not movable',
    'brick5_description' : 'Default',
    'getQR': 'Get QRCode'
  },
  'de': {
    'one_player': 'Ein Spieler',
    'two_player': 'Zwei Spieler',
    'title_login': 'Bitte Namen und Passwort eingeben',
    'title_field1': 'Ziehe das Rechteck auf das weiße Feld',
    'title_field2': 'Drücke die Nummern in der richtigen Reihenfolge...',
    'temp_feedback': 'Deine benötigte Zeit (in ms) beträgt: ',
    'feedback_text': 'ms gebraucht',
    'demo_game' : 'Demo Game',
    'start!' : 'Start!',
    'return_to_menu' : 'Zurück zum Menü',
    'demo_graph' : 'Graphen Demo',
    'demo_ui' : 'UI Demo',
    'demo_fbm' : 'Feedback',
    'demo_lc' : 'Lifecycle',
    'demo_collision' : 'Kollision',
    'demo_movable' : 'Bewegbare Objekte',
    'demo_qrcode' : 'QR Code',
    'demo_inputKeyboards' : 'Input Keyboards',
    'brick1_description' : 'definierte Kreisgröße',
    'brick2_description' : 'Rotiert in Schritten',
    'brick3_description' : 'Rotiert 2x schneller',
    'brick4_description' : 'Unbeweglich',
    'brick5_description' : 'Standard',
    'getQR': 'QRCode'
  }
});
