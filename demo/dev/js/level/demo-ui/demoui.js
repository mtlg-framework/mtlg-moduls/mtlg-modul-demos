/**
 * @Author: thiemo
 * @Date:   2018-09-04T13:04:26+02:00
 * @Last modified by:   thiemo
 * @Last modified time: 2018-09-09T11:45:18+02:00
 */



/*
 * UI Elements Demo
 */
var uiDemo_init = function() {
  var stage = MTLG.getStageContainer();
  MTLG.setBackgroundColor('white');

  with(MTLG.utils.uiElements) {

    var options = MTLG.getOptions();
    //Slider
    var newSlider = addSlider({
      font: "Impact",
      sizeX: 200,
      sizeY: 30,
      firstColor: "red",
      secondColor: "grey",
      min: 0,
      max: 10,
      stepSize: 2,
      defaultVal: 3,
      description: "description",
    }, function(val) {
      console.log("Slider new value: " + val);
    });
    newSlider.x = 50;
    newSlider.y = 100;
    stage.addChild(newSlider);

    //Checkbox
    var newCheckbox = addCheckbox({
      font: "Impact",
      size: 40,
      firstColor: "red",
      secondColor: "grey",
      defaultVal: false,
      description: "Checkbox Test. This is a long test to see what happens when texts are long like this long text which is long.",
    }, function(value) {
      console.log("Checkbox new value:" + value);
    });
    newCheckbox.x = 50;
    newCheckbox.y = 400;
    stage.addChild(newCheckbox);

    //Scrollbox
    var newScroll = addScrollbox({
      font: "Impact",
      sizeX: 300,
      sizeY: 40,
      firstColor: "red",
      secondColor: "grey",
      options: ["Test a very long option to see what will happen now it will contract and change size and such.", "Test1", "Test2", "Test3"],
      defaultVal: 0,
      description: "description",
    }, function(value) {
      console.log("Scrollbox new Value: " + value);
    });
    newScroll.x = 50;
    newScroll.y = 700;
    stage.addChild(newScroll);

    var button = addButton({
      text: "Back to menu",
      sizeX: MTLG.getOptions().width * 0.1,
      sizeY: 70
    }, function() {
      MTLG.lc.goToMenu();
    });
    button.x = options.width / 2;
    button.y = options.height - 100;
    stage.addChild(button);

  }
};

var checkUi = function(gameState) {
  if (gameState && gameState.nextLevel && gameState.nextLevel == "uiDemo") {
    return 1;
  }
  return 0;
}
