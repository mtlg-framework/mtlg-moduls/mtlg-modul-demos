/**
 * @Date:   2019-05-22T15:34:40+02:00
 * @Last modified time: 2019-05-29T10:16:11+02:00
 */


var qrcode_init = function() {
  var stage = MTLG.getStageContainer();
  MTLG.setBackgroundColor('white');

  // Button for creating and adding a qrcode
  let button = MTLG.utils.uiElements.addButton({
      text: MTLG.lang.getString("getQR"),
      sizeX: MTLG.getOptions().width * 0.1,
      sizeY: 70,
      bgColor2: "red"
    },
    function() {
      try {
        //create qrcode and aadd to stage
        MTLG.utils.qrcode.createQRCode('https://mtlg-framework.gitlab.io/', function(result) {
          if(result.success) {
            // get bounds for positioning
            var bounds = result.bitmap.getBounds();
            result.bitmap.x = MTLG.getOptions().width / 2 - bounds.width / 2;
            result.bitmap.y = MTLG.getOptions().height / 2 - bounds.height / 2;

            // add to stage
            stage.addChild(result.bitmap);
            // remove the create button
            stage.removeChild(button);
          } else {
            MTLG.log("QRDemo", "The qrcode couldn't be created.", result.reason);
          }

        }, {width:800}); // set manually options like width
      } catch (e) {
        console.error("You have to include the newest utilities module");
      }
    }
  );
  button.x = MTLG.getOptions().width / 2;
  button.y = button.getBounds().height / 2 + 10;

  stage.addChild(button);

  //Add button to get back to menu
  var backButton = MTLG.utils.uiElements.addButton({
    text: "Back to menu",
    sizeX: MTLG.getOptions().width * 0.1,
    sizeY: 70,
  }, function() {
    MTLG.lc.goToMenu();
  });
  backButton.x = MTLG.getOptions().width / 2;
  backButton.y = MTLG.getOptions().height - 100;
  stage.addChild(backButton);
}



var checkQR = function(gameState) {
  if(gameState && gameState.nextLevel && gameState.nextLevel == "qrDemo"){
    return 1;
  }
  return 0;
}
