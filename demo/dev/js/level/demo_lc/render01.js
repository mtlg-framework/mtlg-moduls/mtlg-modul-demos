/*
 * UI Elements Demo
 * Idea: Create a level structure to describe how lc works.
 * First we will create a math test.
 * If the player answers all questions correctly, they will go to the last level to get feedback.
 * If there are problems with addition, they will go to an addition level.
 * If there are problems with multiplication, they will go to a multiplication level, unless they
 * also have troubles with additions, that level has a higher priority.
 */
var lcDemo_init = function()
{
  var stage = MTLG.getStageContainer();
  MTLG.setBackgroundColor('white');

  //Create an array that holds objects representing the questions
  let questions = [
    {question: "1+1", options: [1,2,3,4], correct: 1},
    {question: "3+6", options: [5,7,9,10], correct: 2},
    {question: "29+35", options: [46,54,63,64], correct: 3},
    {question: "123+321", options: [444,445,555,666], correct: 0},
    {question: "2*6", options: [6,12,18,24], correct: 1},
    {question: "7*7", options: [45,47,49,51], correct: 2},
    {question: "9*8", options: [56,64,68,72], correct: 3},
    {question: "11*25", options: [275, 277, 285, 333], correct: 0},
  ];
  //The answers array will contain the player's answers
  let answers = [0,0,0,0,0,0,0,0];

  //Shorthand for utils
  let utils = MTLG.utils.uiElements;
  //Property for question radio buttons. These options are the same for all questions.
  let qProps = {sizeX : 1000, lineHeight: 30, textColor: "black", firstColor: "red", secondColor: "white"};
  let currentHeight = 50;

  //Construct questionaire: start with a title
  let textP = {text: "Surprise Math Test: Try to answer all questions correctly!", font: currentHeight + "px Arial", color: "Black", sizeX: 1000, sizeY: currentHeight, wrap: true};
  let text = utils.addText(textP);
  text.x = 300;
  text.y = currentHeight;
  stage.addChild(text);
  currentHeight += currentHeight * 1.5;
  //Now create multiple-choice questions and answers for all questions
  for(q of questions){
    qProps.description = q.question;
    qProps.options = q.options;
    let index = questions.indexOf(q);
    let radioB = utils.addRadioButton(qProps, function(newVal){
      answers[index] = newVal;
    });
    radioB.x = 300;
    radioB.y = currentHeight;
    stage.addChild(radioB);
    currentHeight += 3.5*qProps.lineHeight;
  }

  //Create a finish button
  let bProp = {font: 'Arial', text:"Finish", sizeX: 150, sizeY : 70};
  let finishButton = utils.addButton(bProp, function(){
    //The score array contains the user's score, split in addition and multiplication parts
    let score = [0,0];
    //The scores will be compared to the maximum possible score
    let possibleScore = [4, 4];
    for(q in questions){
      if(answers[q] == questions[q].correct){
        if(q < 4){ //addition
          score[0]++;
        }else{ //multiplication
          score[1]++;
        };
      }
    }
    //Now give the information to the other levels
    MTLG.lc.levelFinished({
      nextLevel: "lcDemo",
      score: score,
      possibleScore: possibleScore,
    });
  });
  finishButton.x = 300;
  finishButton.y = currentHeight;
  stage.addChild(finishButton);

};

//The check function determines if this level will be drawn next
//Since this level should be drawn first but not again, it has a low priority but will always return
//greater than zero if the nextLevel is set to lcDemo
var checkLc = function(gameState){
  if(gameState && gameState.nextLevel && gameState.nextLevel == "lcDemo"){
    return 0.01;
  }
  return 0;
}

module.exports = { lcDemo_init, checkLc };
