/**
 * @Author: thiemo
 * @Date:   2018-09-09T11:39:08+02:00
 * @Last modified by:   thiemo
 * @Last modified time: 2018-09-09T11:44:24+02:00
 */



/*
 * Graph Utils Demo
 */
var graphDemo_init = function()
{
  var stage = MTLG.getStageContainer();
  MTLG.setBackgroundColor('white');
  with(MTLG.utils){
    //GRAPH
    //Create a graph with three nodes
    var myGraph = new complexGraph(stage);
    var newNode;
    newNode = myGraph.addNode();
    newNode.addVisualNode(1000,50,stage);
    newNode = myGraph.addNode();
    newNode.addVisualNode(1000,200,stage);
    newNode = myGraph.addNode();
    newNode.addVisualNode(1200,200,stage);

    //Create a second graph with different colors
    var myGraph2 = new complexGraph(stage);
    myGraph2.setColors('red', 'white', 'black'); //edgeCol, nodeBg, nodeText
    var newNode;
    newNode = myGraph2.addNode();
    newNode.addVisualNode(1500,50,stage);
    newNode = myGraph2.addNode();
    newNode.addVisualNode(1500,200,stage);
    newNode = myGraph2.addNode();
    newNode.addVisualNode(1700,200,stage);

    //First create a graph using the stage container to ensure correct scaling
    var myGraph4 = new graph(MTLG.getStageContainer());
    //Add nodes as an array of values
    myGraph4.addNodes([1,2,3,4,5]);
    //Create random edges between nodes
    myGraph4.randomEdges();
    //Add and distribute the nodes evenly on the stage container
    myGraph4.addToStage();
    myGraph4.distributeOnStage();
    //Use the update function to update the visual representation of the graph
    //(This function is also called in distributeOnStage)
    myGraph4.updateGraph();
  }


  //Add button to get back to menu
  var button = MTLG.utils.uiElements.addButton({
    text: "Back to menu",
    sizeX: MTLG.getOptions().width * 0.1,
    sizeY: 70,
  }, function() {
    MTLG.lc.goToMenu();
  });
  button.x = MTLG.getOptions().width / 2;
  button.y = MTLG.getOptions().height - 100;

  stage.addChild(button);
};

var checkGraph = function(gameState){
  if(gameState && gameState.nextLevel && gameState.nextLevel == "graphDemo"){
    return 1;
  }
  return 0;
}
