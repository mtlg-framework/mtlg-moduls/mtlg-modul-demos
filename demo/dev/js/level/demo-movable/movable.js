/**
 * @Date:   2019-10-07T17:12:38+02:00
 * @Last modified time: 2019-10-31T13:39:17+01:00
 */

var checkMovable = function(gameState) {
  if (gameState && gameState.nextLevel && gameState.nextLevel == "movableDemo") {
    return 1;
  }
  return 0;
};

var movableDemo_init = function() {
  var stage = MTLG.getStageContainer();
  var w = MTLG.getOptions().width;
  var h = MTLG.getOptions().height;

  //Add button to get back to menu
  var backButton = MTLG.utils.uiElements.addButton({
    text: "Back to menu",
    sizeX: MTLG.getOptions().width * 0.1,
    sizeY: 70,
  }, function() {
    MTLG.lc.goToMenu();
  });
  backButton.x = w / 2;
  backButton.y = h - 100;

  if (!MTLG.utils.movable) {
    console.error("You have to include the newest utilities module");

  } else {
    // Create movable objects
    //
    var bricks = createBricks();

    tutorial(bricks);
  }



  stage.addChild(backButton);
};

var createBricks = function() {
  var w = MTLG.getOptions().width;
  var h = MTLG.getOptions().height;
  var stage = MTLG.getStageContainer();
  var brickSize = 70;

  var defaults = {
    font: "Arial",
    sizeY: brickSize * 2 / 4,
    textColor: "black",
    bgColor1: "white",
    bgColor2: "black",
  }

  // Create Brick 1
  var brick1 = createBrickObj(brickSize, [
    [0, 1, 1],
    [1, 1, 0],
    [0, 1, 0]
  ], "#FF0000");
  brick1.x = w * 1 / 4;
  brick1.y = h * 1 / 2;

  // add description
  var text1 = MTLG.utils.gfx.getText(MTLG.lang.getString("brick1_description"), defaults.sizeY + 'px ' + defaults.font, defaults.textColor);
  text1.y = -defaults.sizeY;
  brick1.addChild(text1);

  stage.addChild(brick1);
  // default movable
  var brick1_movable = new MTLG.utils.movable.Movable(brick1, brickSize * (3 / 2), brickSize * (3 / 2));
  brick1_movable.setColor_innerCircle("#00ff00"); // special color for inner circle
  brick1_movable.setColor_outerCircle("#0000ff"); // special color for outer circle
  brick1_movable.setDistance(400); // changes the size of inner and outer circle


  // create brick 2
  var brick2 = createBrickObj(brickSize, [
    [1, 1, 1, 1],
    [1, 0, 0, 0]
  ], "#0000FF");
  brick2.x = w * 2 / 4;
  brick2.y = h * 1 / 2;

  // add description
  var text2 = MTLG.utils.gfx.getText(MTLG.lang.getString("brick2_description"), defaults.sizeY + 'px ' + defaults.font, defaults.textColor);
  text2.y = -defaults.sizeY;
  brick2.addChild(text2);

  stage.addChild(brick2);
  var brick2_movable = new MTLG.utils.movable.Movable(brick2, brickSize * (4 / 2), brickSize * 1);
  brick2_movable.setAlpha_innerCircle(0.5); // change the alpha value of inner circle
  brick2_movable.setAlpha_outerCircle(0.5); // change the alpha value of outer circle
  brick2_movable.setFactorOuterCircle(1.5); // change the size of the outer circle
  brick2_movable.rotate_in_steps(2); // each step is 90 degree (360/(2∗steps))


  // create brick 3 which is a copy of brick 2 but with other rotate behaviour
  var brick3 = createBrickObj(brickSize, [
    [1, 1, 1, 1],
    [1, 0, 0, 0]
  ], "#0000FF");
  brick3.x = w * 2 / 4;
  brick3.y = h * 3 / 4;

  // add description
  var text3 = MTLG.utils.gfx.getText(MTLG.lang.getString("brick3_description"), defaults.sizeY + 'px ' + defaults.font, defaults.textColor);
  text3.y = -defaults.sizeY;
  brick3.addChild(text3);

  stage.addChild(brick3);
  var brick3_movable = new MTLG.utils.movable.Movable(brick3, brickSize * (4 / 2), brickSize * 1);
  brick3_movable.setAlpha_innerCircle(0.5);
  brick3_movable.setAlpha_outerCircle(0.5);
  brick3_movable.setFactorOuterCircle(1.5);
  brick3_movable.setJumps(2); //roteates two times faster


  // create brick 4
  var brick4 = createBrickObj(brickSize, [
    [1, 0, 0],
    [1, 1, 1],
    [0, 1, 0]
  ], "#FF00FF");
  brick4.x = w * 3 / 4;
  brick4.y = h * 1 / 2;

  // add description
  var text4 = MTLG.utils.gfx.getText(MTLG.lang.getString("brick4_description"), defaults.sizeY + 'px ' + defaults.font, defaults.textColor);
  text4.y = -defaults.sizeY;
  brick4.addChild(text4);

  stage.addChild(brick4);
  var brick4_movable = new MTLG.utils.movable.Movable(brick4, brickSize * (3 / 2), brickSize * (3 / 2));
  brick4_movable.setAllowToMove(false); // Disables moving
  brick4_movable.setInnerCircle_shadow("#FF0000"); // unimportant, because the circle is never displayed due to missing movibilty
  brick4_movable.moveFormTo(w * 0.9, h * 0.2); // transfers the brick to the given position


  // create brick5
  var brick5 = createBrickObj(brickSize, [
    [1, 0, 0],
    [1, 1, 1],
    [0, 1, 0]
  ], "#FF00FF");
  brick5.x = w * 3 / 4;
  brick5.y = h * 3 / 4;

  // add description
  var text5 = MTLG.utils.gfx.getText(MTLG.lang.getString("brick5_description"), defaults.sizeY + 'px ' + defaults.font, defaults.textColor);
  text5.y = -defaults.sizeY;
  brick5.addChild(text5);

  stage.addChild(brick5);
  new MTLG.utils.movable.Movable(brick5, brickSize * (3 / 2), brickSize * (3 / 2));
  // totally default!

  return {
    brick1,
    brick2,
    brick3,
    brick4,
    brick5
  };
}

var createBrickObj = (size, gridArray, color) => {
  var container = new createjs.Container();

  for (let i = 0; i < gridArray.length; i++) {
    for (let j = 0; j < gridArray[i].length; j++) {
      if (gridArray[i][j] == "1") {
        var shape = new createjs.Shape();
        shape.graphics.setStrokeStyle(2);
        shape.graphics.beginStroke("#000000");
        shape.graphics.beginFill(color);
        shape.graphics.drawRect((j * size), (i * size), size, size);
        container.addChild(shape);
      }
    }
  }
  return container;
};


var tutorial = function({
  brick1,
  brick2,
  brick3,
  brick4,
  brick5
}) {
  var stage = MTLG.getStageContainer();

  //create Buttons
  var rotateButton = MTLG.utils.uiElements.addButton({
    text: "Show how to rotate",
    sizeX: MTLG.getOptions().width * 0.15,
    sizeY: 70,
  }, function() {
    makeFeedbackForRotating(brick1);
  });
  rotateButton.x = 20 + (1 / 2) * (MTLG.getOptions().width * 0.15);
  rotateButton.y = 20 + 70 / 2;
  stage.addChild(rotateButton);

  var flipButton = MTLG.utils.uiElements.addButton({
    text: "Show how to flip",
    sizeX: MTLG.getOptions().width * 0.15,
    sizeY: 70,
  }, function() {
    makeFeedbackForFlipping(brick1);
  });
  flipButton.x = 20 + (1 / 2) * (MTLG.getOptions().width * 0.15);
  flipButton.y = 20 + 70 / 2 + 90;
  stage.addChild(flipButton);

  async function makeFeedbackForFlipping(brick1) {
    return new Promise(resolve => {
      var w = MTLG.getOptions().width;
      var h = MTLG.getOptions().height;
      // Toch animation
      var movetotarget = MTLG.utils.fbm.createContent("movetotarget");
      movetotarget.pointCounter = 1;
      movetotarget.pointStartX = brick1.x / w;
      movetotarget.pointStartY = brick1.y / h;
      movetotarget.pointEndX = brick1.x / w;
      movetotarget.pointEndY = brick1.y / h;
      movetotarget.pointRotation = 0;
      movetotarget.pointSpeed = 0;
      movetotarget.pointSize = 150;

      var movetotarget2 = MTLG.utils.fbm.createContent("movetotarget2");
      movetotarget2.pointCounter = 2;
      movetotarget2.pointDelay = 400;
      movetotarget2.pointStartX = (brick1.x + 100) / w;
      movetotarget2.pointStartY = brick1.y / h;
      movetotarget2.pointEndX = (brick1.x + 300) / w;
      movetotarget2.pointEndY = brick1.y / h;
      movetotarget2.pointRotation = 0;
      movetotarget2.pointSpeed = 300;
      movetotarget2.pointSize = 150;

      //General Timing
      var overallTiming = MTLG.utils.fbm.createTiming("overallTiming");
      overallTiming.movable = false;
      overallTiming.movableMini = false;
      overallTiming.openDuration = 0;
      overallTiming.miniDuration = 0;
      overallTiming.totalDuration = 0;
      overallTiming.closeable = false;
      overallTiming.requestable = true;
      overallTiming.minimizable = true;
      overallTiming.reqPointer = false;
      overallTiming.rotateOnStart = true;

      //General Style
      var neutralStyle = MTLG.utils.fbm.createStyle("neutralStyle");
      neutralStyle.screenLocked = true;
      neutralStyle.fontSize = null;
      neutralStyle.focus = 0;
      neutralStyle.fontType = "Arial";
      neutralStyle.textAlign = "center";
      neutralStyle.color = 'LightCyan';
      neutralStyle.bordercolor = 'LightBlue';
      neutralStyle.buttonColor = 'LightCyan';
      neutralStyle.curve = 0.5;

      setTimeout(function() {
        var feedback1 = MTLG.utils.fbm.giveFeedback(brick1, "movetotarget", "overallTiming", "neutralStyle");

        feedback1.hand.scaleX *= -1; // flip the hand vertically

        // delete the fixed hand
        setTimeout(() => {
          feedback1.hand.deleteFB();
          resolve();
        }, 3000);
      }, 300);


      setTimeout(function() {
        MTLG.utils.fbm.giveFeedback(brick1, "movetotarget2", "overallTiming", "neutralStyle");
      }, 700);
    });
  }

  async function makeFeedbackForRotating(brick1) {
    return new Promise(resolve => {
      var w = MTLG.getOptions().width;
      var h = MTLG.getOptions().height;
      // Toch animation
      var movetotarget = MTLG.utils.fbm.createContent("movetotarget");
      movetotarget.pointCounter = 1;
      movetotarget.pointStartX = brick1.x / w;
      movetotarget.pointStartY = brick1.y / h;
      movetotarget.pointEndX = brick1.x / w;
      movetotarget.pointEndY = brick1.y / h;
      movetotarget.pointRotation = 0;
      movetotarget.pointSpeed = 0;
      movetotarget.pointSize = 150;

      var movetotarget2 = MTLG.utils.fbm.createContent("movetotarget2");
      movetotarget2.pointCounter = 2;
      movetotarget2.pointDelay = 400;
      movetotarget2.pointStartX = brick1.x / w;
      movetotarget2.pointStartY = brick1.y / h;
      movetotarget2.pointEndX = brick1.x / w;
      movetotarget2.pointEndY = (brick1.y + 100) / h;
      movetotarget2.pointRotation = 0;
      movetotarget2.pointSpeed = 0;
      movetotarget2.pointSize = 150;

      //General Timing
      var overallTiming = MTLG.utils.fbm.createTiming("overallTiming");
      overallTiming.movable = false;
      overallTiming.movableMini = false;
      overallTiming.openDuration = 0;
      overallTiming.miniDuration = 0;
      overallTiming.totalDuration = 0;
      overallTiming.closeable = false;
      overallTiming.requestable = true;
      overallTiming.minimizable = true;
      overallTiming.reqPointer = false;
      overallTiming.rotateOnStart = true;

      //General Style
      var neutralStyle = MTLG.utils.fbm.createStyle("neutralStyle");
      neutralStyle.screenLocked = true;
      neutralStyle.fontSize = null;
      neutralStyle.focus = 0;
      neutralStyle.fontType = "Arial";
      neutralStyle.textAlign = "center";
      neutralStyle.color = 'LightCyan';
      neutralStyle.bordercolor = 'LightBlue';
      neutralStyle.buttonColor = 'LightCyan';
      neutralStyle.curve = 0.5;

      setTimeout(function() {
        var feedback1 = MTLG.utils.fbm.giveFeedback(brick1, "movetotarget", "overallTiming", "neutralStyle");

        feedback1.hand.scaleX *= -1; // flip the hand vertically

        // delete the fixed hand
        setTimeout(() => {
          feedback1.hand.deleteFB();
          resolve();
        }, 4400);
      }, 300);


      setTimeout(function() {
        var feedback2 = MTLG.utils.fbm.giveFeedback(brick1, "movetotarget2", "overallTiming", "neutralStyle");

        moveCircleHand(feedback2.hand, brick1, 200);
      }, 700);
    });
  }

  var moveCircleHand = function(hand, target, r) {
    var centerX = target.x;
    var centerY = target.y;
    var speed = 600;

    if (hand.counter > 0) {
      //Startposition
      hand.x = centerX + (1 / 4) * r;
      hand.y = centerY - (3 / 4) * r;
      hand.visible = true;

      //Bewegung zur Endposition
      createjs.Tween.get(hand, {
        loop: false
      }).to({
        x: centerX + (1 / 4) * r
      }, speed, createjs.Ease.sineIn).to({
        x: centerX + (3 / 4) * r
      }, speed, createjs.Ease.sineOut).to({
        x: centerX + (1 / 4) * r
      }, speed, createjs.Ease.sineIn).call(function() {
        hand.visible = false;
      });
      createjs.Tween.get(hand, {
        loop: false
      }).to({
        y: centerY - (3 / 4) * r
      }, speed, createjs.Ease.sineOut).to({
        y: centerY
      }, speed, createjs.Ease.sineIn).to({
        y: centerY + (3 / 4) * r
      }, speed, createjs.Ease.sineOut).call(function() {
        hand.counter--;
        hand.visible = false;
        setTimeout(moveCircleHand, 1 + hand.delay, hand, target, r); //Da unsichtbarkeit länger dauern kann, als positionsveränderung (hand zuckt sonst rum)
      });

    } else {
      hand.deleteFB();
    }
  };


};
