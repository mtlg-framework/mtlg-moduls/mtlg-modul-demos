/*
 * UI Elements Demo
 * This demo was created to demonstrate the rectangle and pixel collision tests.
 * These can be found in the MTLG utilities:
 * MTLG.utils.collision
 * The two objects are draggable, if their rectangles overlap, the dragged objects alpha is set to 0.5
 * If they also overlap on some pixel, the dragged objects alpha is further reduced to 0.1
 * Notice how the rectangle collision is less precise for objects that are not non-rotated rectangles.
*/
var collisionDemo_init = function(){
  var stage = MTLG.getStageContainer();
  MTLG.setBackgroundColor('white');

  //Create Shorthand for collision utiliy
  let coll = MTLG.utils.collision;

  //Create a bitmap
  let someShape = MTLG.assets.getBitmap("img/Objekt A Blue.png");
  someShape.regX = coll.getBounds(someShape).width/2;
  someShape.regY = coll.getBounds(someShape).height/2;
  someShape.on("pressmove", function(evt){
    someShape.set(stage.globalToLocal(evt.stageX, evt.stageY));
    //This is the pixel collision test
    if(coll.checkPixelCollision(someShape, secondShape)){
      someShape.alpha = 0.1;
    //This is the rectangle collision test
    }else if(coll.checkRectCollision(secondShape,someShape)){
      someShape.alpha = 0.5;
    }else{
      someShape.alpha = 1;
    }
  });
  stage.addChild(someShape);

  //Create second bitmap, same as above
  let secondShape = MTLG.assets.getBitmap("img/Objekt A Blue.png");
  secondShape.regX = coll.getBounds(secondShape).width/2;
  secondShape.regY = coll.getBounds(secondShape).height/2;
  secondShape.rotation = 45;
  secondShape.on("pressmove", function(evt){
    secondShape.set(stage.globalToLocal(evt.stageX, evt.stageY));
    if(coll.checkPixelCollision(someShape, secondShape)){
      secondShape.alpha = 0.1;
    }else if(coll.checkRectCollision(secondShape,someShape)){
      secondShape.alpha = 0.5;
    }else{
      secondShape.alpha = 1;
    }
  });
  stage.addChild(secondShape);

  //Also add a Back button to return to menu
  let options = MTLG.getOptions();
  let prop = {font: 'Arial', text:"Back", sizeX: options.width / 10, sizeY : 70};
  var button = MTLG.utils.uiElements.addButton(prop, function(){MTLG.utils.inactivity.removeAllListeners();MTLG.lc.goToMenu();});
  button.x = options.width / 2;
  button.y = options.height / 2;
  stage.addChild(button);

};

// This function checks if the demo should be rendered next, it has nothing to do with collisions
var checkCollision = function(gameState){
  if(gameState && gameState.nextLevel && gameState.nextLevel == "collisionDemo"){
    return 1;
  }
  return 0;
}

module.exports = { collisionDemo_init, checkCollision };
