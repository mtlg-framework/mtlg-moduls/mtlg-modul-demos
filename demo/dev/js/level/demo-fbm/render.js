/**
 * @Author: thiemo
 * @Date:   2018-09-09T11:39:08+02:00
 * @Last modified by:   thiemo
 * @Last modified time: 2018-09-09T11:44:00+02:00
 */



var fbmDemo_init = function() {
  var stage = MTLG.getStageContainer();
  var options = MTLG.getOptions();
  //Hintergrund
  var background = MTLG.utils.gfx.getShape();
  background.graphics.beginFill('white').drawRect(0, 0, options.width, options.height);
  coordinates = {
    xCoord: options.width / 2,
    yCoord: options.height / 2
  };
  background.regX = options.width / 2;
  background.regY = options.height / 2;
  background.x = coordinates.xCoord;
  background.y = coordinates.yCoord;

  //Add button to get back to menu
  var button = MTLG.utils.uiElements.addButton({
    text: "Back to menu",
    sizeX: MTLG.getOptions().width * 0.1,
    sizeY: 70,
  }, function() {
    MTLG.lc.goToMenu();
  });
  button.x = options.width / 2;
  button.y = options.height - 100;

  //Beispielobjekt A
  var movebuttonA = MTLG.assets.getBitmap("img/Objekt A DarkBlue.png");
  var width = (options.width * 0.1);
  movebuttonA.scaleX = movebuttonA.scaleY = width / 500;

  var movelabelA = MTLG.utils.gfx.getText("Objekt A", '25px Arial', 'black');
  movelabelA.textAlign = 'center';
  movelabelA.textBaseline = "middle";
  movelabelA.x = movelabelA.y = width / 2;

  var movecontainerA = new createjs.Container();
  movecontainerA.name = "Objekt A";
  movecontainerA.addChild(movebuttonA, movelabelA);
  movecontainerA.regX = 0.5 * width;
  movecontainerA.regY = 0.5 * width;
  movecontainerA.x = options.width / 4 + 0.5 * width;
  movecontainerA.y = options.height / 3 - width / 2 + 0.5 * width;
  movecontainerA.setBounds(0, 0, width, width);
  movecontainerA.feedbackY = -0.125;

  //Interaktionen von Objekt A
  var movefeedback = false;
  movecontainerA.on('pressmove', function({
    localX,
    localY
  }) {
    movecontainerA.set(movecontainerA.localToLocal(localX, localY, movecontainerA.parent));
  });
  movecontainerA.on("pressup", function(evt) {
    var distanceX = Math.abs(movecontainerA.x - targetcontainer.x);
    var distanceY = Math.abs(movecontainerA.y - targetcontainer.y);
    if ((distanceX < 1.5 * width) && (distanceY < 1.5 * width)) {
      if ((distanceX < 0.1 * width) && (distanceY < 0.1 * width)) {
        MTLG.utils.fbm.giveFeedback(targetcontainer, "TargetHit", "congratsTiming", "targetStyle"); //Anzeigen von Feedback wenn Ziel getroffen wurde
      } else {
        //lightParse Event um festzusstellen, wenn Objekt wiederholt nah zum Ziel ist
        if (!movefeedback) {
          MTLG.lightParse.matchEvent({
            user: 1,
            action: "NearTarget",
            time: ((new Date).getTime())
          })
        };
      }
    } else {
      //lightParse Event um festzusstellen, wenn Objekt wiederholt nicht näher zum Ziel kommt
      if (!movefeedback) {
        MTLG.lightParse.matchEvent({
          user: 1,
          action: "FailMovement",
          time: ((new Date).getTime())
        })
      }; //lightParse Event
    }
  });

  //Beispielobjekt B
  var movebuttonB = MTLG.assets.getBitmap("img/Objekt B DarkBlue.png");
  movebuttonB.scaleX = movebuttonB.scaleY = width / 500;

  var movelabelB = MTLG.utils.gfx.getText("Objekt B", '25px Arial', 'black');
  movelabelB.textAlign = 'center';
  movelabelB.textBaseline = "middle";
  movelabelB.x = movelabelB.y = width / 2;

  var movecontainerB = new createjs.Container();
  movecontainerB.name = "Objekt B";
  movecontainerB.addChild(movebuttonB, movelabelB);
  movecontainerB.regX = 0.5 * width;
  movecontainerB.regY = 0.5 * width;
  movecontainerB.x = options.width / 4 + 0.5 * width;;
  movecontainerB.y = 2 * options.height / 3;
  movecontainerB.setBounds(0, 0, width, width);
  movecontainerB.feedbackY = -0.125;

  //Interaktionen von Objekt B
  var movefeedback = false;
  movecontainerB.on('pressmove', function({
    localX,
    localY
  }) {
    movecontainerB.set(movecontainerB.localToLocal(localX, localY, movecontainerB.parent));
  });
  movecontainerB.on("pressup", function(evt) {
    var distanceX = Math.abs(movecontainerB.x - targetcontainer.x);
    var distanceY = Math.abs(movecontainerB.y - targetcontainer.y);
    //lightParse Event um festzusstellen, wenn falsches Objekt wiederholt bewegt wird
    if (!movefeedback) {
      MTLG.lightParse.matchEvent({
        user: 1,
        action: "CircleMoved",
        time: ((new Date).getTime())
      })
    };
    if ((distanceX < 0.5 * width) && (distanceY < 0.5 * width)) {
      //Anzeigen von Feedback wenn Ziel mit falschem Objekt getroffen wurde
      MTLG.utils.fbm.giveFeedback(movecontainerB, "TargetHitCircle", "moveTiming", "targetStyle");
    }
  });

  //Beispiel Zielobjekt
  var targetarea = MTLG.assets.getBitmap("img/Objekt A Blue.png");
  targetarea.scaleX = targetarea.scaleY = width / 500;

  var targetlabel = MTLG.utils.gfx.getText("Ziel", '25px Arial', 'black');
  targetlabel.textAlign = 'center';
  targetlabel.textBaseline = "middle";
  targetlabel.x = targetlabel.y = width / 2;

  var targetcontainer = new createjs.Container();
  targetcontainer.name = "Ziel";
  targetcontainer.addChild(targetarea, targetlabel);
  targetcontainer.x = 3 * options.width / 4;
  targetcontainer.y = options.height / 2 - width / 2;
  targetcontainer.setBounds(0, 0, width, width);
  targetcontainer.feedbackY = -0.13;
  targetcontainer.regX = width / 2;
  targetcontainer.regY = width / 2;
  targetcontainer.rotation = 0;

  //Speichert ob Feedback auf dem Ziel angezeigt wird und ob Maus gedrückt ist auf Ziel
  var targetfeedback = false;
  var targetmouse = false;

  //Interaktion des Zielobjekts
  targetcontainer.on("mousedown", function(evt) {
    targetmouse = true;
  });
  targetcontainer.on("pressmove", function(evt) {
    //Anzeigen von Feedback wenn Spieler probiert Ziel zu bewegen
    MTLG.utils.fbm.giveFeedback(targetcontainer, "TargetClick", "targetTiming", "targetStyle");
    if (targetmouse) {
      //lightParse Event um festzusstellen, wenn Spieler wiederholt probiert das Ziel zu bewegen
      MTLG.lightParse.matchEvent({
        user: 1,
        action: "TargetClicked",
        time: ((new Date).getTime())
      });
      targetmouse = false;
    }
  });
  targetcontainer.on("pressup", function(evt) {
    targetmouse = false;
  });

  stage.addChild(background, button, targetcontainer, movecontainerA, movecontainerB);

  //lightParse

  //Deklaration lightParse Patterns
  var patternMove = "SEQ(x1,x2,x3) WHERE (x1.user = x2.user, x2.user = x3.user, x1.action = 'FailMovement', x2.action = 'FailMovement', x3.action = 'FailMovement')"; //3x richtiges Objekt nicht zum Ziel
  var patternWrongObject = "SEQ(x1,x2) WHERE (x1.user = x2.user, x1.action = 'CircleMoved', x2.action = 'CircleMoved')"; //2x falsches Objekt
  var patternNearTarget = "SEQ(x1,x2) WHERE (x1.user = x2.user, x1.action = 'NearTarget', x2.action = 'NearTarget')"; //2x richtiges Objekt nah zum Ziel
  var patternTarget3Times = "SEQ(x1,x2,x3) WHERE (x1.user = x2.user, x2.user = x3.user, x1.action = 'TargetClicked', x2.action = 'TargetClicked', x3.action = 'TargetClicked')"; //3x Versuch Ziel zu bewegen

  //Hinzufügen zu lightParse (gleiche Reihenfolge)
  MTLG.lightParse.addPattern(patternMove, function(evt) {
    MTLG.utils.fbm.giveFeedback(movecontainerA, "FailMovement", "moveTiming", "buttonStyle");
  });
  MTLG.lightParse.addPattern(patternWrongObject, function(evt) {
    MTLG.utils.fbm.giveFeedback(movecontainerA, "wrongObject", "moveTiming", "buttonStyle");
  });
  MTLG.lightParse.addPattern(patternNearTarget, function(evt) {
    MTLG.utils.fbm.giveFeedback(movecontainerA, "CloseToTarget", "moveTiming", "buttonStyleNoFocus");
  });
  MTLG.lightParse.addPattern(patternTarget3Times, function(evt) {
    if (!targetfeedback) {
      MTLG.utils.fbm.giveFeedback(targetcontainer, "TargetClicked3Times", "targetTiming", "targetStyle");
    }
  });

  //FBM

  //Deklaration der Feedback-Inhalte

  //statische Anleitung
  var startHelp = MTLG.utils.fbm.createContent("startHelp");
  startHelp.text = "Bringe das passende Objekt ins Ziel";
  startHelp.img = "img/TestBild.png";
  startHelp.textX = 0;
  startHelp.textY = 0;
  startHelp.textWidth = 0.75;
  startHelp.textHeight = 1;
  startHelp.imgX = 0.9;
  startHelp.imgY = 0.5;
  startHelp.imgWidth = 0.25;
  startHelp.imgHeight = 0.8;
  startHelp.imgRatioLocked = true;
  startHelp.totalWidth = 0.2;
  startHelp.totalHeight = 0.05;

  //Falls Objekt 3 mal erfolglos bewegt wurde
  var failmovementContent = MTLG.utils.fbm.createContent("FailMovement");
  failmovementContent.text = "Bringe das Objekt ins Ziel";
  failmovementContent.textWidth = 1;
  failmovementContent.textHeight = 1;
  failmovementContent.totalWidth = 0.1;
  failmovementContent.totalHeight = 0.05;
  failmovementContent.flair = "neutral";
  failmovementContent.flairTime = 0;
  failmovementContent.flairSize = 200;

  //Falls falsches Objekt 2 mal bewegt wurde
  var wrongObject = MTLG.utils.fbm.createContent("wrongObject");
  wrongObject.text = "Versuch es mal hiermit"
  wrongObject.totalWidth = 0.1;
  wrongObject.totalHeight = 0.05;
  wrongObject.flair = "neutral";
  wrongObject.flairTime = 0;
  wrongObject.flairSize = 200;

  //Animation der Touch-Geste
  var movetotarget = MTLG.utils.fbm.createContent("movetotarget");
  movetotarget.pointCounter = 1;
  movetotarget.pointStartX = movecontainerA.x / options.width;
  movetotarget.pointStartY = movecontainerA.y / options.height;
  movetotarget.pointEndX = null;
  movetotarget.pointEndY = null;
  movetotarget.pointRotation = 0;
  movetotarget.pointSpeed = 400;
  movetotarget.pointSize = 200;

  //Wenn richtiges Objekt in der Nähe des Ziels ist
  var CloseToTargetContent = MTLG.utils.fbm.createContent("CloseToTarget");
  CloseToTargetContent.text = "Du hast es fast geschafft"
  CloseToTargetContent.totalWidth = 0.1;
  CloseToTargetContent.totalHeight = 0.05;

  //Wenn richtiges Objekt ins Ziel gebracht wurde
  var TargetHitContent = MTLG.utils.fbm.createContent("TargetHit");
  TargetHitContent.text = "Volltreffer!"
  TargetHitContent.sound = "positive";
  TargetHitContent.flair = "positive";
  TargetHitContent.flairSize = 120;
  TargetHitContent.flairTime = 0.3;
  TargetHitContent.totalWidth = 0.1;
  TargetHitContent.totalHeight = 0.05;

  //Wenn falsches Objekt ins Ziel gebracht wurde
  var TargetHitCircleContent = MTLG.utils.fbm.createContent("TargetHitCircle");
  TargetHitCircleContent.flair = "negative";
  TargetHitCircleContent.flairSize = 120;
  TargetHitCircleContent.flairTime = 0.3;
  TargetHitCircleContent.sound = "negative";

  //Wenn Spieler probiert Ziel zu bewegen
  var targetclickedContent = MTLG.utils.fbm.createContent("TargetClick");
  targetclickedContent.text = null;
  targetclickedContent.flair = "negative";
  targetclickedContent.flairSize = 120;
  targetclickedContent.flairTime = 0.2;
  targetclickedContent.flairIntensity = 1;

  //Wenn Spieler wiederholt probiert Ziel zu bewegen
  var targetclicked3timesContent = MTLG.utils.fbm.createContent("TargetClicked3Times");
  targetclicked3timesContent.text = "Du kannst das Ziel nicht bewegen";
  targetclicked3timesContent.totalWidth = 0.1;
  targetclicked3timesContent.totalHeight = 0.05;

  //Deklaration Timing-Templates

  //Generelles Timing
  var overallTiming = MTLG.utils.fbm.createTiming("overallTiming");
  overallTiming.movable = false;
  overallTiming.movableMini = false;
  overallTiming.openDuration = 0;
  overallTiming.miniDuration = 0;
  overallTiming.totalDuration = 0;
  overallTiming.closeable = false;
  overallTiming.requestable = true;
  overallTiming.minimizable = true;
  overallTiming.reqPointer = false;
  overallTiming.rotateOnStart = true;

  //Timing bei Zielobjekt
  var targetTiming = MTLG.utils.fbm.createTiming("targetTiming");
  targetTiming.movable = true;
  targetTiming.closeable = true;
  targetTiming.requestable = true;
  targetTiming.reqPointer = true;
  targetTiming.minimizable = true;
  targetTiming.rotateOnStart = false;
  targetTiming.openCallback = function() {
    targetfeedback = true
  };
  targetTiming.closeCallback = function() {
    targetfeedback = false
  };

  //Timing bei Gratulation
  var congratsTiming = MTLG.utils.fbm.getCopyTiming("moveTiming"); //als Vorlage
  congratsTiming.totalDuration = 2;
  congratsTiming.movable = false;
  congratsTiming.closeable = false;
  congratsTiming.minimizable = false;
  MTLG.utils.fbm.setTiming(congratsTiming, "congratsTiming"); //muss noch gespeichert werden

  //Timing bei Hinweisen zu beweglichen Elementen
  var moveTiming = MTLG.utils.fbm.createTiming("moveTiming");
  moveTiming.totalDuration = 5;
  moveTiming.movable = true;
  moveTiming.closeable = true;
  moveTiming.requestable = false;
  moveTiming.reqPointer = true;
  moveTiming.minimizable = false;
  moveTiming.rotateOnStart = false;
  moveTiming.openCallback = function() {
    movefeedback = true;
  };
  moveTiming.closeCallback = function() {
    movefeedback = false;
  };

  //Deklaration Style-Templates

  //Genereller Style
  var neutralStyle = MTLG.utils.fbm.createStyle("neutralStyle");
  neutralStyle.screenLocked = true;
  neutralStyle.fontSize = null;
  neutralStyle.focus = 0;
  neutralStyle.fontType = "Arial";
  neutralStyle.textAlign = "center";
  neutralStyle.color = 'LightCyan';
  neutralStyle.bordercolor = 'LightBlue';
  neutralStyle.buttonColor = 'LightCyan';
  neutralStyle.curve = 0.5;

  //Style des Zielobjekts
  var targetStyle = MTLG.utils.fbm.createStyle("targetStyle");
  targetStyle.fontSize = 25;
  targetStyle.fontType = "Arial";
  targetStyle.textAlign = "center";
  targetStyle.color = 'LightCyan';
  targetStyle.curve = 0.5;
  targetStyle.theme = "Dark";

  //Style der beweglichen Objekte
  var buttonStyle = MTLG.utils.fbm.createStyle("buttonStyle");
  buttonStyle.screenLocked = true;
  buttonStyle.fontSize = 25;
  buttonStyle.fontType = "Arial";
  buttonStyle.textAlign = "center";
  buttonStyle.focus = 1;
  buttonStyle.color = 'DeepSkyBlue';
  buttonStyle.bordercolor = 'SteelBlue';
  buttonStyle.buttonColor = 'DeepSkyBlue';
  buttonStyle.curve = 0.5;

  //Style bewegliche Elemente ohne Fokus
  var buttonStyleNoFocus = MTLG.utils.fbm.getCopyStyle("buttonStyle");
  buttonStyleNoFocus.focus = 0;
  MTLG.utils.fbm.setStyle(buttonStyleNoFocus, "buttonStyleNoFocus");

  //Manuelle Zielpositionen unabhängig von Spielobjekten
  //Mitte oben
  var TopScreen = MTLG.utils.fbm.createTarget("TopScreen");
  TopScreen.y = 0.06;
  TopScreen.x = 0.5;

  //Mitte unten
  var BotScreen = MTLG.utils.fbm.getCopyTarget("TopScreen");
  BotScreen.y = 0.94;
  MTLG.utils.fbm.setTarget(BotScreen, "BotScreen");

  //Statische Hinweise
  //Werden direkt beim Start angezeigt -> unabhängig von lightParse
  MTLG.utils.fbm.giveFeedback("TopScreen", "startHelp", "overallTiming", "neutralStyle");
  MTLG.utils.fbm.giveFeedback("BotScreen", "startHelp", "overallTiming", "neutralStyle");
  MTLG.utils.fbm.giveFeedback(targetcontainer, "movetotarget", "overallTiming", "neutralStyle")

}

var checkFbm = function(gameState) {
  if (gameState && gameState.nextLevel && gameState.nextLevel == "fbmDemo") {
    return 1;
  }
  return 0;
}
