/**
 * @Date:   2020-03-05T10:42:03+01:00
 * @Last modified time: 2020-03-26T15:26:55+01:00
 */


 var inputkeyboard_init = function() {
   var stage = MTLG.getStageContainer();
   MTLG.setBackgroundColor('white');

   // InputField for creating and adding a input
   let options = {
     x: 100,
     y: 100,
     w: 500,
     h: 50,
     placeholder: "Type your input after clicking"
   }
   let customNotPrintingKeys = [
     "Meta", "F1", "F2", "F3", "F4", "F5", "F6", "F7", "F8", "F9", "F10", "F11",
     "F12", "NumLock", "ArrowUp", "ArrowLeft", "ArrowDown", "ArrowRight",
     "Home", "End", "PageUp", "PageDown", "Clear", "Insert", "Delete", "Enter",
     "PrintScreen", "ScrollLock", "Pause", "Dead", "Alt", "Escape",
     "Shift", "CapsLock", "Control", "AltGraph", "Backspace", "NumLock", "ContextMenu"
   ];
   let button1 = MTLG.utils.inputkeyboards.getInputField(options, new MTLG.utils.inputkeyboards.Methods.HardwareKeyboard(customNotPrintingKeys));
   stage.addChild(button1);

   options = {
     x: 700,
     y: 100,
     w: 500,
     h: 50,
     placeholder: "Type your input after clicking"
   }
   let button1_2 = MTLG.utils.inputkeyboards.getInputField(options, new MTLG.utils.inputkeyboards.Methods.HardwareKeyboard());
   stage.addChild(button1_2);

   // InputField for creating and adding a input
   options = {
     x: 100,
     y: 300,
     w: 500,
     h: 50,
     placeholder: "CLick for custom keyboard"
   }
   let virtualKeyboard = new MTLG.utils.inputkeyboards.Methods.VirtualKeyboard();
   virtualKeyboard.addLayout("test", [["1", "2"], ["3", "4"]]);
   virtualKeyboard.setLayout("test");
   let button2 = MTLG.utils.inputkeyboards.getInputField(options, virtualKeyboard);
   stage.addChild(button2);

   options = {
     x: 700,
     y: 300,
     w: 500,
     h: 50,
     placeholder: "Click for original keyboard"
   }
   let virtualKeyboard_2 = new MTLG.utils.inputkeyboards.Methods.VirtualKeyboard("original");
   let button2_2 = MTLG.utils.inputkeyboards.getInputField(options, virtualKeyboard_2);
   stage.addChild(button2_2);

   options = {
     x: 100,
     y: 500,
     w: 500,
     h: 50,
     placeholder: "Click for alphabet keyboard"
   }
   let virtualKeyboard_3 = new MTLG.utils.inputkeyboards.Methods.VirtualKeyboard("alphabet");
   let button2_3 = MTLG.utils.inputkeyboards.getInputField(options, virtualKeyboard_3);
   stage.addChild(button2_3);

   options = {
     x: 700,
     y: 500,
     w: 500,
     h: 50,
     placeholder: "Click for number keyboard",
     enterPress: false
   }
   let virtualKeyboard_4 = new MTLG.utils.inputkeyboards.Methods.VirtualKeyboard("numbers");
   let button2_4 = MTLG.utils.inputkeyboards.getInputField(options, virtualKeyboard_4);
   stage.addChild(button2_4);

   // InputField for creating and adding a input
   options = {
     x: 100,
     y: 700,
     w: 500,
     h: 50,
     placeholder: "click for open code"
   }
   let button3 = MTLG.utils.inputkeyboards.getInputField(options, new MTLG.utils.inputkeyboards.Methods.DistributedDisplayKeyboard());
   stage.addChild(button3);

   options = {
     x: 700,
     y: 700,
     w: 500,
     h: 50,
     placeholder: "click for open qr code",
     enterButton: true
   }
   let button4 = MTLG.utils.inputkeyboards.getInputField(options, new MTLG.utils.inputkeyboards.Methods.DistributedDisplayKeyboard());
   stage.addChild(button4);

   //Add button to get back to menu
   var backButton = MTLG.utils.uiElements.addButton({
     text: "Back to menu",
     sizeX: MTLG.getOptions().width * 0.1,
     sizeY: 70,
   }, function() {
     MTLG.lc.goToMenu();
   });
   backButton.x = MTLG.getOptions().width / 2;
   backButton.y = MTLG.getOptions().height - 100;
   stage.addChild(backButton);
 }



 var checkInputKeyboards = function(gameState) {
   if(gameState && gameState.nextLevel && gameState.nextLevel == "inputKeyboardsDemo"){
     return 1;
   }
   return 0;
 }
