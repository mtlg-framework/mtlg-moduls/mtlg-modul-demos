/**
 * @Author: thiemo
 * @Date:   2018-08-30T13:52:18+02:00
 * @Last modified by:
 * @Last modified time: 2020-03-18T12:10:07+01:00
 */



// expose framework for debugging
window.MTLG = MTLG;
// ignite the framework
document.addEventListener("DOMContentLoaded", MTLG.init);

// add moduls
require('mtlg-modul-menu');
require('mtlg-modul-lightparse');
require('mtlg-moduls-utilities');
require('mtlg-module-dd');

// load configuration
require('../manifest/device.config.js');
require('../manifest/game.config.js');
require('../manifest/game.settings.js');

// load translations
require('../lang/lang.js');

/**
 * Init function for the game.
 * @params options: The options parsed from the manifest files/ Defaults.
 */
var initGame = function(pOptions){
  //Initialize game

  //Initialize levels and Menus:
  //The MTLG framework uses the lifecycle manager (lc) to handle level progression,
  //resetting levels and to define a starting point for the game.
  //Use the functions registerMenu and registerLevel to define your levels.
  //The starting point of the game will be the registered menu, if one is present,
  //or the game with the highest value when passed the empty game state.
  //Register Menu

  MTLG.lc.registerMenu(drawMainMenu);

  //Register levels
  //Levels consist of two parts: the first parameter is the entrance function,
  //that sets the level up and renders it. The second parameter as a check function,
  //that receives the current gamestate as paramter and returns a value between 0 and 1.
  //The lifecycle manager starts the level with the highest return value in the next step.

  // Register Basic Demo
  MTLG.lc.registerLevel(field1_init, checkLevel1);
  // Register ui Demo
  MTLG.lc.registerLevel(uiDemo_init, checkUi);
  // Register Graph Demo
  MTLG.lc.registerLevel(graphDemo_init, checkGraph);
  // Register FBM Demo
  MTLG.lc.registerLevel(fbmDemo_init, checkFbm);
  // Register lc Demo
  MTLG.lc.registerLevel(lcDemo_init, checkLc);
  MTLG.lc.registerLevel(lcLastDemo_init, checkLcLast);
  MTLG.lc.registerLevel(lcMultDemo_init, checkLcMult);
  MTLG.lc.registerLevel(lcPlusDemo_init, checkLcPlus);
  // Register collision Demo
  MTLG.lc.registerLevel(collisionDemo_init, checkCollision);

  // Register Movable Demo
  MTLG.lc.registerLevel(movableDemo_init, checkMovable);

  // Register qrCode Demo
  MTLG.lc.registerLevel(qrcode_init, checkQR);

  //Register inputKeyboards demo
  MTLG.lc.registerLevel(inputkeyboard_init, checkInputKeyboards);

  //Init is done
  console.log("Game Init function called");
}

//Register Init function with the MTLG framework
//The function passed to addGameInit will be used to initialize the game.
//Use the initialization to register levels and menus.
MTLG.addGameInit(initGame);
