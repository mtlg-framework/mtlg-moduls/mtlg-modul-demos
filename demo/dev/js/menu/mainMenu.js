/**
 * @Author: thiemo
 * @Date:   2018-09-09T11:39:08+02:00
 * @Last modified by:
 * @Last modified time: 2020-03-05T10:43:48+01:00
 */


var drawMainMenu = function() {

  // log in demo player if not yet present
  if (!MTLG.getPlayerNumber()) {
      console.log("Logging in");
      MTLG.lc.goToLogin(); //Leave Main Menu and go to login
  }

  // title
  let text = MTLG.lang.getString("demo_game");
  let title = MTLG.utils.gfx.getText(text, '50px Arial', 'black');
  title.x = MTLG.getOptions().width / 2;
  title.y = MTLG.getOptions().height / 4;
  title.regX = title.getBounds().width / 2;
  title.regY = title.getBounds().height / 2;
  title.textBaseline = 'alphabetic';

  //create buttons for each demo.
  var buttons = getButtonsForDemos({
    "demo_game" : "basicDemo",
    "demo_graph" : "graphDemo",
    "demo_ui" : "uiDemo",
    "demo_fbm" : "fbmDemo",
    "demo_lc" : "lcDemo",
    "demo_collision" : "collisionDemo",
    "demo_movable" : "movableDemo",
    "demo_qrcode" : "qrDemo",
    "demo_inputKeyboards": "inputKeyboardsDemo",
  }, MTLG.getOptions().width * 0.15, 70, 30, 30, MTLG.getOptions().width / 2, MTLG.getOptions().height / 3, 6);

  var stopMusic = MTLG.utils.uiElements.addButton({
      font: "Arial",
      text: "Stop Music",
      sizeX: MTLG.getOptions().width * 0.1,
      sizeY: 70,
      textColor: "black",
      bgColor1: "white",
      bgColor2: "red"
    },
    function() {
      MTLG.assets.stopBackgroundMusic();
    }
  );
  stopMusic.x = MTLG.getOptions().width - 100;
  stopMusic.y = MTLG.getOptions().height - 100;

  //Sound Demo
  //Play the "Welcome" Sound first, then start the Background music
  //The "Welcome" Sound is a language sound, so it will play in the current language
  var soundInstance = MTLG.assets.playLangSound("mainMenu/hello");
  soundInstance.on("complete", function() {
    MTLG.assets.playBackgroundMusic("bgm/the_field_of_dreams");
    MTLG.assets.setBackgroundMusicVolume(0.5);
  }, this);



  // set white Background
  MTLG.setBackgroundColor("white");

  //get stage object to add objects
  var stage = MTLG.getStageContainer();

  // add objects to stage
  stage.addChild(title);

  // add the created demo buttons to stage
  for(var index in buttons) {
    stage.addChild(buttons[index]);
  }
  stage.addChild(stopMusic);
}

/**
 * Creates buttons from the given object and orders them as a table
 *
 * @param  {object} demos        Dictionary for the different demos. Key => Value: LangName => nextLevelString
 * @param  {type} buttonWidth  The width of the buttons
 * @param  {type} buttonHeight The height of the buttons
 * @param  {type} widthMargin  Space between buttons horizontal.
 * @param  {type} heightMargin Space between buttons vertical.
 * @param  {type} startx       The x-coordinate of the middle of the table on the stage
 * @param  {type} starty       The starting point iin y-direction.
 * @param  {type} row_count    Number of max rows
 * @return {Array}              returns an array of buttons.
 */
var getButtonsForDemos = function (demos, buttonWidth, buttonHeight, widthMargin, heightMargin, startx, starty, row_count) {
  var ret = [];
  // get number of buttons that should be created
  var count = Object.keys(demos).length;

  // get number of columns based on the given number of rows and the total number of buttons
  var col_count = parseInt(count / row_count) + 1;

  // calculate the start x position.
  if(col_count%2 == 0) {
    startx -= ((col_count / 2)-(1/2))*(buttonWidth + widthMargin);
  } else if(col_count != 1) {
    startx -= parseInt(col_count / 2)*(buttonWidth +widthMargin);
  }

  var x = startx;
  var y = starty;

  var i_x = 0;
  var i_y = 0;
  for(var name in demos) {
    (function(lang, nextLevel) {
      // create button
      var button = MTLG.utils.uiElements.addButton({
          font: "Arial",
          text: MTLG.lang.getString(lang),
          sizeX: buttonWidth,
          sizeY: buttonHeight,
          textColor: "black",
          bgColor1: "white",
          bgColor2: "red"
        },
        function() {
          MTLG.lc.levelFinished({
            nextLevel: nextLevel
          });
        }
      );

      button.x = x;
      button.y = y;

      // increase indicies and position
      i_x++;
      x = startx + (i_x * (widthMargin+buttonWidth))%(col_count * (widthMargin+buttonWidth));
      if ( x == startx) {
        i_y++;
        y = starty + (i_y * (heightMargin+buttonHeight))%(row_count * (heightMargin+buttonHeight));
      }

      ret.push(button);
    })(name, demos[name]);

  }
  return ret;
}
